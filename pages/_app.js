import "../styles/globals.css"
import MenuLayout from "../src/components/MenuLayout"
import { ConfigProvider } from "antd"
import { decodeJWT } from "../src/components/services/utils"

const App = ({ Component, pageProps, ...otherProps }) => {
    return (
        <div>
            <ConfigProvider
                theme={{
                    token: {
                        colorPrimary: "#007E85",
                        colorLink: "#007E85",
                        colorLinkHover: "#6EAB36",
                        colorLinkActive: "#6EAB36",
                    },
                }}
            >
                <MenuLayout>
                    <Component {...pageProps} {...otherProps} />
                </MenuLayout>
            </ConfigProvider>
        </div>
    )
}

export default App
