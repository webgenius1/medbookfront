import { Typography } from "antd"
import { decodeJWT, getJWT } from "../src/components/services/utils"

const { Text, Title } = Typography

export default function Home() {
    return <Title>Home</Title>
}
