import { Typography } from "antd"

const { Title } = Typography
const Search = () => {
    return <Title>Search</Title>
}

export default Search
