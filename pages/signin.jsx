import { LockOutlined, UserOutlined } from "@ant-design/icons"
import { Button, Checkbox, Form, Input, message, Space, Typography } from "antd"
import Link from "next/link"
import axios from "axios"
import { useRouter } from "next/router"
import { useEffect } from "react"

const { Title } = Typography
//TODO: ajouter un deco si connecté
const SignIn = () => {
    const router = useRouter()

    useEffect(() => {
        if (localStorage.getItem("jwt")) {
            router.push("/")
        }
    }, [])

    const onFinish = async (values) => {
        await axios
            .post(
                `${process.env.NEXT_PUBLIC_API_BASE_URL}/sessions/sign-in`,
                values
            )
            .then((res) => {
                localStorage.setItem("jwt", res.data.jwt)
                message.success("Vous êtes connecté")
                router.push("/")
            })
            .catch((err) => {
                console.log(err)
                message.error(err?.response?.data?.error)
            })
        console.log("Success:", values)
    }

    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo)
    }
    return (
        <Space direction="vertical" size="middle" style={{ display: "flex" }}>
            <Title>Sign In</Title>
            <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: "S'il vous plaît entrez votre email!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Mot de passe"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message:
                                "S'il vous plaît entrez votre mot de passe!",
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Se connecter
                    </Button>
                </Form.Item>
            </Form>
        </Space>
    )
}
export default SignIn
