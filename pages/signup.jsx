import {
    Button,
    DatePicker,
    Form,
    Input,
    message,
    Select,
    Space,
    Typography,
} from "antd"
import dayjs from "dayjs"
import axios from "axios"
import { getBasicAuthConfig } from "../src/components/services/utils"
import { useContext } from "react"
import { useRouter } from "next/router"

const { Title } = Typography

const Signup = () => {
    const router = useRouter()
    const onFinish = async (fieldsValues) => {
        const values = {
            ...fieldsValues,
            date_of_birth: fieldsValues["date_of_birth"].format("DD-MM-YYYY"),
        }

        await axios
            .post(
                `${process.env.NEXT_PUBLIC_API_BASE_URL}/sessions/sign-up`,
                values
            )
            .then((res) => {
                console.log(res)
                message.success(res.data.message)
                router.push("/signin")
            })
            .catch((err) => {
                console.log(err)
                message.error(err?.response?.data?.error)
            })
    }
    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo)
    }

    return (
        <Space direction="vertical" size="middle" style={{ display: "flex" }}>
            <Title>Sign Up</Title>
            <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            type: "email",
                            message: "L'e-mail saisie n'est pas valide!",
                        },
                        {
                            required: true,
                            message: "S'il vous plaît entrez votre email!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Mot de passe"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Please input your password!",
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    name="confirm_password"
                    label="Confirmez le mot de passe"
                    dependencies={["password"]}
                    rules={[
                        {
                            required: true,
                            message:
                                "S'il vous plaît confirmez votre mot de passe!",
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (
                                    !value ||
                                    getFieldValue("password") === value
                                ) {
                                    return Promise.resolve()
                                }
                                return Promise.reject(
                                    new Error(
                                        "Les deux mots de passe que vous avez saisis ne correspondent pas!"
                                    )
                                )
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="Prénom"
                    name="firstname"
                    rules={[
                        {
                            required: true,
                            message: "S'il vous plaît entrez votre prénom!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Nom de famille"
                    name="lastname"
                    rules={[
                        {
                            required: true,
                            message:
                                "S'il vous plaît entrez votre nom de famille!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Genre"
                    name="sex"
                    rules={[
                        {
                            required: true,
                            message: "S'il vous plaît entrez votre genre!",
                        },
                    ]}
                >
                    <Select
                        placeholder="Sélectionnez votre genre"
                        options={[
                            {
                                label: "Homme",
                                value: "Homme",
                            },
                            {
                                label: "Femme",
                                value: "Femme",
                            },
                        ]}
                    />
                </Form.Item>

                <Form.Item
                    label="Téléphone"
                    name="phone_number"
                    rules={[
                        {
                            required: true,
                            message:
                                "S'il vous plaît entrez votre numéro de téléphone!",
                        },
                    ]}
                >
                    <Input addonBefore="+33" />
                </Form.Item>

                <Form.Item
                    label="Adresse"
                    name="address"
                    rules={[
                        {
                            required: true,
                            message: "S'il vous plaît entrez votre adresse!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Ville"
                    name="city"
                    rules={[
                        {
                            required: true,
                            message: "S'il vous plaît entrez votre ville!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Code Postal"
                    name="postal_code"
                    rules={[
                        {
                            required: true,
                            message:
                                "S'il vous plaît entrez votre code postal!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Date de naissance"
                    name="date_of_birth"
                    rules={[
                        {
                            required: true,
                            message:
                                "S'il vous plaît entrez votre date de naissance!",
                        },
                    ]}
                >
                    <DatePicker
                        placeholder={dayjs().format("YYYY-MM-DD")}
                        format={"YYYY-MM-DD"}
                        disabledDate={(current) =>
                            dayjs(current).isAfter(dayjs())
                        }
                    />
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        S'inscrire
                    </Button>
                </Form.Item>
            </Form>
        </Space>
    )
}
export default Signup
