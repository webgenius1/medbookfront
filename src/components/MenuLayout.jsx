import {
    Breadcrumb,
    Button,
    Col,
    Layout,
    Menu,
    Row,
    Spin,
    Typography,
} from "antd"
import { Fragment, useEffect, useState } from "react"
import { useRouter } from "next/router"
import { decodeJWT, getJWT, signOut } from "./services/utils"
import {
    UserOutlined,
    SearchOutlined,
    DesktopOutlined,
    HomeOutlined,
    PieChartOutlined,
} from "@ant-design/icons"

const { Header, Content, Footer, Sider } = Layout

const { Title } = Typography

const getItem = (label, key, icon, children, type) => {
    return {
        key,
        children,
        icon,
        label,
        type,
    }
}

//items don't contain sign up and sign in buttons
const logoItem = [
    {
        label: <img src="./logo.png" alt="Logo" width={"120px"} />,
    },
]

const items = [
    {
        label: "Accueil",
        key: "home",
    },
    {
        label: "Service",
        key: "service",
    },
    {
        label: "Nous Contacter",
        key: "contactus",
    },
]

const itemsNotLogged = [
    {
        label: (
            <Button type="link" size="large">
                Inscription
            </Button>
        ),
        key: "signup",
    },
    {
        label: (
            <Button type="primary" size="default">
                Connexion
            </Button>
        ),
        key: "signin",
    },
]

const itemsLogged = [
    {
        label: "Accueil",
        key: "home",
        icon: <HomeOutlined />,
    },
    {
        label: "Rechercher un médecin",
        key: "search",
        icon: <SearchOutlined />,
    },
    {
        label: "Mon Compte",
        key: "account",
        icon: <UserOutlined />,
    },
]

const MenuLayout = ({ children }) => {
    const [current, setCurrent] = useState("mail")
    const [isLoading, setIsLoading] = useState(true)
    const router = useRouter()

    useEffect(() => {
        setCurrent(router.pathname === "/" ? "home" : router.pathname.slice(1))
    }, [router.pathname])

    useEffect(() => {
        while (typeof window === "undefined") {}
        setIsLoading(false)
    }, [])

    const onClick = (e) => {
        console.log("click ", e)
        setCurrent(e.key)
        e.key === "logout"
            ? signOut()
            : e.key === "home"
            ? router.push("/")
            : router.push(`/${e.key}`)
    }

    if (isLoading)
        return (
            <Title>
                <Spin />
            </Title>
        )

    return !getJWT() ? (
        <div>
            <Row>
                <Col span={10}>
                    <Menu
                        onClick={onClick}
                        selectedKeys={[current]}
                        mode="horizontal"
                        items={logoItem}
                    ></Menu>
                </Col>
                <Col span={9}>
                    <Menu
                        onClick={onClick}
                        selectedKeys={[current]}
                        mode="horizontal"
                        items={items}
                    />
                </Col>
                <Col span={5}>
                    <Menu
                        onClick={onClick}
                        selectedKeys={[current]}
                        mode="horizontal"
                        items={itemsNotLogged}
                    />
                </Col>
            </Row>
        </div>
    ) : (
        <Layout
            hasSider
            style={{
                marginTop: -20,
                minHeight: "100vh",
            }}
        >
            <Sider
                collapsed
                style={{
                    overflow: "auto",
                    height: "100vh",
                    position: "fixed",
                    left: 0,
                    top: 0,
                    bottom: 0,
                }}
            >
                <div className="demo-logo-vertical" />
                <Menu
                    onClick={onClick}
                    theme="dark"
                    mode="inline"
                    defaultSelectedKeys={["home"]}
                    items={itemsLogged}
                />
            </Sider>
            <Layout
                className="site-layout"
                style={{
                    marginLeft: 200,
                    marginTop: 64,
                }}
            >
                <Content
                    style={{
                        margin: "24px 16px 0",
                        overflow: "initial",
                    }}
                >
                    <div
                        style={{
                            padding: 24,
                            textAlign: "center",
                        }}
                    >
                        {children}
                    </div>
                </Content>
            </Layout>
        </Layout>
    )
}
export default MenuLayout
