const getBasicAuthConfig = () => {
    const token =
        typeof window !== "undefined" ? localStorage.getItem("jwt") : null

    return {
        headers: { authentication: token },
    }
}

const getJWT = () => {
    return typeof window !== "undefined" ? localStorage.getItem("jwt") : null
}

const decodeJWT = () => {
    const jwt =
        typeof window !== "undefined" ? localStorage.getItem("jwt") : null

    if (jwt) {
        const base64Url = jwt.split(".")[1]
        const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/")
        const jsonPayload = decodeURIComponent(
            atob(base64)
                .split("")
                .map((c) => {
                    return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2)
                })
                .join("")
        )

        return JSON.parse(jsonPayload).payload
    }
}

const signOut = () => {
    localStorage.removeItem("jwt")
    window.location.href = "/"
}

export { getBasicAuthConfig, getJWT, decodeJWT, signOut }
